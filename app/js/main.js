require.config({
    paths           : {
        'bootstrap' : '../bower_components/bootstrap/dist/js/bootstrap.min',
        'jquery'    : '../bower_components/jquery/dist/jquery.min'
    },

    shim            : {
        'bootstrap' : [
            'jquery'
        ]
    }
});

define( function ( require ) {
    'use strict'

    require( 'bootstrap' );
    console.log( 'application loaded' );
})